module gitlab.com/zj-gitlab/bundlecache

go 1.13

require (
	github.com/aws/aws-sdk-go v1.19.45
	github.com/spf13/cobra v0.0.5
	github.com/urfave/cli v1.22.2
	github.com/urfave/cli/v2 v2.0.0
	gocloud.dev v0.18.0
)
