package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Name:  "bundlecache",
		Usage: "create repository bundle to increase CI speed",
		Flags: flags,
		Commands: []*cli.Command{
			{
				Name:   "create",
				Usage:  "get the repository, create and upload the bundle to object storage",
				Action: bundleRepo,
			},
			{
				Name:   "extract",
				Usage:  "download and extract the repositoy bundle, must run in GitLab-CI",
				Action: extractRepo,
			},
		},
	}

	if err := app.Run(os.Args); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var flags = []cli.Flag{
	&cli.StringFlag{
		Name:    "host",
		Value:   "https://gitlab.com",
		Usage:   "Set the GitLab host to clone from",
		EnvVars: []string{"CI_SERVER_HOST"},
	},
	&cli.StringFlag{
		Name:  "gitlab-user",
		Value: "gitlab-ci-token",
		Usage: "Set the clone user for private projects",
	},
	&cli.StringFlag{
		Name:    "gitlab-token",
		Usage:   "Set the access token for GitLab",
		EnvVars: []string{"CI_JOB_TOKEN"},
	},
	&cli.StringFlag{
		Name:    "bucket",
		Usage:   "set the bucket with scheme: e.g. s3://gitlab-bundle-cache-bucket",
		EnvVars: []string{"GCS_BUCKET", "AWS_BUCKET"},
	},
	&cli.StringFlag{
		Name:    "aws-access-key-id",
		Usage:   "set the aws access key",
		EnvVars: []string{"AWS_ACCESS_KEY_ID"},
	},
	&cli.StringFlag{
		Name:    "aws-secret-access-key",
		Usage:   "set the token used to access the bucket",
		EnvVars: []string{"AWS_SECRET_ACCESS_KEY"},
	},
	&cli.StringFlag{
		Name:    "bucket-region",
		Usage:   "set the region for the bucket",
		EnvVars: []string{"AWS_REGION"},
	},
}
